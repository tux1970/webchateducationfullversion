<!DOCTYPE html>
<html>
	<head>
		<title>RPI - Scuola Media Fermi</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>

	<body>
		<table>
			<td>
				<div class="w3-container">

					<div class="w3-container w3-center">
						<h3>Your Image</h3>
						<img src="../images/img_avatar3.png" alt="Avatar" style="width:50%" id="mainImage">

						<form class="w3-container">
							<table>
								<tr>
									<td>
										<label>Nick Name</label>
									</td>
									<td>
										<input class="w3-input" type="text" id="nickName">
									</td>
								</tr>

								<tr>
									<td>
										<div class="w3-padding w3-xlarge w3-text-orange" >
											<i class="fa fa-male" id="cliccamiImage1"></i>
											<i class="fa fa-male" id="cliccamiImage2"></i>
											<i class="fa fa-female" id="cliccamiImage3"></i>
											<i class="fa fa-female" id="cliccamiImage4"></i>
										</div>
									</td>
								</tr>
							</table>
						</form> 

						<div class="w3-section w3-center">
							<button class="w3-button w3-green" id="save">Save</button>
						</div>

					</div>
				</div>
			</td>

			<td>
				<table summary="logo" width="100%">
					<tr>
						<td width="800" align="center"></td>
						<td width="800"  align="right"><h4>Web@Chat 1.1 - E. Fermi</h4></td>
					</tr>
				</table>

				<form name=main action=mainPage.php method=POST>
					<table width="100%">
						<tr>
							<td>
								<iframe name="view" src="view.php" height="770px" width="100%" scrolling="yes" ></iframe>
							</td>
							<td>
								<iframe name="insert" src="insert.php" height="770px" width="100%" scrolling="yes" ></iframe>
							</td>
						</tr>
					</table>
				</form>
			</td>


		</table>

        <script>
            var cliccamiSave = document.getElementById("save");
			
            
			cliccamiSave.addEventListener("click", function()
			{
				var nickName = document.getElementById("nickName").value;
				var mainImage = document.getElementById("mainImage").src;
				setCookie("nickName",nickName,10);
				setCookie("mainImage",mainImage,10);

				alert("i save your personal data "); 

  	        });

			cliccamiImage1.addEventListener("click", function()
			{
				document.getElementById("mainImage").src = "../images/img_avatar2.png";
            });

			cliccamiImage2.addEventListener("click", function()
			{
				document.getElementById("mainImage").src = "../images/img_avatar3.png";
            });

			cliccamiImage3.addEventListener("click", function()
			{
				document.getElementById("mainImage").src = "../images/img_avatar5.png";
            });

			cliccamiImage4.addEventListener("click", function()
			{
				document.getElementById("mainImage").src = "../images/img_avatar6.png";
            });


			function setCookie(cname, cvalue, exdays) {
				var d = new Date();
				d.setTime(d.getTime() + (exdays*24*60*60*1000));
				var expires = "expires="+ d.toUTCString();
				document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
			}

			function getCookie(cname) {
				var name = cname + "=";
				var decodedCookie = decodeURIComponent(document.cookie);
				var ca = decodedCookie.split(';');
				for(var i = 0; i <ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == ' ') {
					c = c.substring(1);
					}
					if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
					}
				}
				return "";
			}
			
        </script>
	</body>
</html>

