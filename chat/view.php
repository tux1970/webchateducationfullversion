<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
	<title>RPI - Scuola Media Fermi</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Description" content="" />
</head>


<body>

	<?php

		$servername = "172.105.28.200";
		$username = "root";
		$password = "";
		$dbname = "scuola";

		$conn = new mysqli($servername, $username, $password,$dbname);

		// Check connection
		if ($conn->connect_error) 
		{
			die("Connection failed: " . $conn->connect_error);
		}

		
		print "<form name=view action=view.php method=GET>";

		//Selezione delle chat
		$query = "select * from new_news order by new_id desc";
		$result = $conn->query($query);
		$number = $result->num_rows;

		print "<br>";
		print "<table style=width:100%>";

		if ($result->num_rows > 0) 
		{
			while($row = $result->fetch_assoc()) 
			{
				$new_nick = $row["new_nick"];
				$new_message = $row["new_message"];
				$new_image = $row["new_image"];

				print"<tr><td colspan=10>Nick Name : $new_nick</td></tr>";
				print"<tr><td><textarea name=new_notizie readonly=true cols=80 rows=4>$new_message</textarea></td>";
				print"<td><img src = $new_image  width=42 height=42>";
				print"</tr>";
			}
		}

		print "</table>";
		print "</form>";

		// Close the database connection
		$conn->close();

	?>

	<script>

		var formName = "view"; // form name
		var timeLen = 1000; // 60 seconds

		function submitForm() 
		{
				document.forms[formName].submit();
		}
		window.setTimeout("submitForm()",timeLen);

	</script>

</body>
</html>

